package com.dwd.tripaccountantAPI.DOM;

import com.dwd.tripaccountantAPI.db.Base;
import com.dwd.tripaccountantAPI.models.Expense;
import com.dwd.tripaccountantAPI.models.Trip;
import com.dwd.tripaccountantAPI.models.User;
import com.dwd.tripaccountantAPI.utils.Helper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guilherme on 31/03/14.
 */
public class ExpenseDOM {

    public static Expense createNewExpense(User user, Trip trip, float amount, String descr){
        Expense expense = null;
        String sql = "INSERT INTO expenses VALUES (?,?,?,?,?)";

        PreparedStatement stmt = null;
        String randomUUID = Helper.generateRandomUUID();

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);

            stmt.setString(1, randomUUID);
            stmt.setString(2, descr);
            stmt.setFloat(3, amount);
            stmt.setString(4, user.getUuid());
            stmt.setString(5, trip.getUuid());

            if (stmt.executeUpdate() != 0);
            expense = new Expense(user, trip, randomUUID, amount, descr);

            Base.closeDbConnection();

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return expense;
    }

    public static Expense findExpenseById(String uuid){
        Expense expense = null;
        String sql = "SELECT uuid, description, amount, user_uuid, trip_uuid FROM expenses WHERE uuid = ? LIMIT 1";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, uuid);
            rs = stmt.executeQuery();

            while(rs.next()){
                String expenseUuid = rs.getString("uuid");
                String descr = rs.getString("description");
                float amount = rs.getFloat("amount");
                String userUuid = rs.getString("user_uuid");
                String tripUuid = rs.getString("trip_uuid");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                // Find the trip
                Trip trip = TripDOM.findTripById(tripUuid);

                expense = new Expense(user, trip, expenseUuid, amount, descr);
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return expense;
    }

    public static List<Expense> getAllExpenses(){
        List<Expense> expenses = new ArrayList<Expense>();
        String sql = "SELECT uuid, description, amount, user_uuid, trip_uuid FROM expenses";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            rs = stmt.executeQuery();

            while(rs.next()){
                String expenseUuid = rs.getString("uuid");
                String descr = rs.getString("description");
                float amount = rs.getFloat("amount");
                String userUuid = rs.getString("user_uuid");
                String tripUuid = rs.getString("trip_uuid");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                // Find the trip
                Trip trip = TripDOM.findTripById(tripUuid);

                expenses.add(new Expense(user, trip, expenseUuid, amount, descr));
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return expenses;
    }

    public static List<Expense> getAllExpensesByUserId(String uuid) {
        List<Expense> expenses = new ArrayList<Expense>();
        String sql = "SELECT uuid, description, amount, user_uuid, trip_uuid FROM expenses WHERE user_uuid = ?";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, uuid);
            rs = stmt.executeQuery();

            while(rs.next()){
                String expenseUuid = rs.getString("uuid");
                String descr = rs.getString("description");
                float amount = rs.getFloat("amount");
                String userUuid = rs.getString("user_uuid");
                String tripUuid = rs.getString("trip_uuid");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                // Find the trip
                Trip trip = TripDOM.findTripById(tripUuid);

                expenses.add(new Expense(user, trip, expenseUuid, amount, descr));
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return expenses;
    }

}
