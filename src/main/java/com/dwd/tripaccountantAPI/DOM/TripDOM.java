package com.dwd.tripaccountantAPI.DOM;

import com.dwd.tripaccountantAPI.db.Base;
import com.dwd.tripaccountantAPI.models.Trip;
import com.dwd.tripaccountantAPI.models.User;
import com.dwd.tripaccountantAPI.utils.Helper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by guilherme on 31/03/14.
 */
public class TripDOM {

    public static Trip createNewTrip(User user, String city,
                                     String country, Date startDate,
                                     int numOfDays){
        Trip trip = null;
        String sql = "INSERT INTO trips VALUES (?,?,?,?,?,?)";

        PreparedStatement stmt = null;

        String randomUUID = Helper.generateRandomUUID();

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);

            stmt.setString(1, randomUUID);
            stmt.setString(2, city);
            stmt.setString(3, country);

            if (startDate == null)
                startDate = new Date();

            stmt.setDate(4, new java.sql.Date(startDate.getTime()));

            stmt.setInt(5, numOfDays);
            stmt.setString(6, user.getUuid());

            if (stmt.executeUpdate() != 0);
                trip = new Trip(randomUUID, country, city, startDate,
                        numOfDays, user);

            Base.closeDbConnection();

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return trip;
    }

    public static Trip findTripById(String uuid){
        Trip trip = null;
        String sql = "SELECT uuid, city, country, start_date, num_of_days, user_uuid FROM trips WHERE uuid = ? LIMIT 1";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, uuid);

            rs = stmt.executeQuery();

            while(rs.next()){
                String tripUuid = rs.getString("uuid");
                String city = rs.getString("city");
                String country = rs.getString("country");

                Date startDate = null;

                if(rs.getDate("start_date") != null)
                    startDate = new Date(rs.getDate("start_date").getTime());

                int numOfDays = rs.getInt("num_of_days");
                String userUuid = rs.getString("user_uuid");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                trip = new Trip(tripUuid, country, city, startDate, numOfDays, user);
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return trip;
    }

    public static List<Trip> getAllTrips(){
        List<Trip> trips = new ArrayList<Trip>();
        String sql = "SELECT uuid, city, country, start_date, num_of_days, user_uuid FROM trips";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            rs = stmt.executeQuery();

            while(rs.next()){
                String tripUuid = rs.getString("uuid");
                String city = rs.getString("city");
                String country = rs.getString("country");

                Date startDate = null;

                if(rs.getDate("start_date") != null)
                    startDate = new Date(rs.getDate("start_date").getTime());

                int numOfDays = rs.getInt("num_of_days");
                String userUuid = rs.getString("user_uuid");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                trips.add(new Trip(tripUuid, country, city, startDate, numOfDays, user));
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return trips;
    }

    public static List<Trip> getAllTripsByUserId(String userUuid){
        List<Trip> trips = new ArrayList<Trip>();
        String sql = "SELECT uuid, city, country, start_date, num_of_days, user_uuid FROM trips WHERE user_uuid = ?";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, userUuid);
            rs = stmt.executeQuery();

            while(rs.next()){
                String tripUuid = rs.getString("uuid");
                String city = rs.getString("city");
                String country = rs.getString("country");

                Date startDate = null;

                if(rs.getDate("start_date") != null)
                    startDate = new Date(rs.getDate("start_date").getTime());

                int numOfDays = rs.getInt("num_of_days");

                // Find the user
                User user = UserDOM.findUserById(userUuid);

                trips.add(new Trip(tripUuid, country, city, startDate, numOfDays, user));
            }
            Base.closeDbConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return trips;
    }
}