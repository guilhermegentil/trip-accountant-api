package com.dwd.tripaccountantAPI.DOM;

import com.dwd.tripaccountantAPI.db.Base;
import com.dwd.tripaccountantAPI.models.User;
import com.dwd.tripaccountantAPI.utils.Helper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by guilherme on 30/03/14.
 */
public class UserDOM {

    /*
     * As every request requires an instance of User, create it
     * as an class member to save one new allocation on every
     * method call.
     */
    private static User user = null;

    /**
     *
     * @param fullName
     * @param email
     * @return
     */
    public static User createNewUser(String fullName, String email){
        user = null;
        String sql = "INSERT INTO users VALUES (?,?,?,?)";

        PreparedStatement stmt = null;

        String randomUUID = Helper.generateRandomUUID();
        String randomToken = Helper.generateRandomUUID();

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);

            stmt.setString(1, randomUUID);
            stmt.setString(2, fullName);
            stmt.setString(3, email);
            stmt.setString(4, randomToken);

            if (stmt.executeUpdate() != 0);
                user = new User(randomUUID,fullName, email, randomToken);

            Base.closeDbConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /**
     *
     * @param uuid
     * @return
     */
    public static User findUserById(String uuid){
        user = null;
        String sql = "SELECT uuid, full_name, email, api_token FROM users WHERE uuid = ? LIMIT 1";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, uuid);

            rs = stmt.executeQuery();

            while (rs.next()){
                String userUuid = rs.getString("uuid");
                String fullName = rs.getString("full_name");
                String email = rs.getString("email");
                String apiToken = rs.getString("api_token");

                user = new User(userUuid, fullName, email, apiToken);
            }

            Base.closeDbConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /**
     *
     * @param email
     * @return
     */
    public static User findUserByEmail(String email){
        user = null;
        String sql = "SELECT uuid, full_name, email, api_token FROM users WHERE email = ? LIMIT 1";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, email);

            rs = stmt.executeQuery();

            while (rs.next()){
                String userUuid = rs.getString("uuid");
                String fullName = rs.getString("full_name");
                String userEmail = rs.getString("email");
                String apiToken = rs.getString("api_token");

                user = new User(userUuid, fullName, userEmail, apiToken);
            }

            Base.closeDbConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /**
     *
     * @param token
     * @return
     */
    public static User findUserByToken(String token){
        user = null;
        String sql = "SELECT uuid, full_name, email, api_token FROM users WHERE api_token = ?";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = Base.getDbConnection().prepareStatement(sql);
            stmt.setString(1, token);

            rs = stmt.executeQuery();

            while (rs.next()){
                String userUuid = rs.getString("uuid");
                String fullName = rs.getString("full_name");
                String email = rs.getString("email");
                String apiToken = rs.getString("api_token");

                user = new User(userUuid, fullName, email, apiToken);
            }

            Base.closeDbConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
}
