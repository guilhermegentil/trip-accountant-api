package com.dwd.tripaccountantAPI;


import com.dwd.tripaccountantAPI.DOM.ExpenseDOM;
import com.dwd.tripaccountantAPI.models.Expense;
import com.dwd.tripaccountantAPI.utils.ErrorModel;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Root resource (exposed at "expense" path)
 */
@Path("/expense")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ExpenseAPI {

    private Gson gson = new Gson();

    /**
     * Get details about a specific trip
     * @param uuid
     * @return
     */
    @GET
    @Path("{uuid}")
    public String getExpense(@PathParam(value = "uuid") final String uuid) {

        Expense expense = null;

        if (uuid != null) {
            expense = ExpenseDOM.findExpenseById(uuid);
        } else {
            ErrorModel error = new ErrorModel(404, "Please specify either the UUID");
            return gson.toJson(error);
        }

        if (expense == null) {
            ErrorModel error = new ErrorModel(404, "Resource not found!");
            return gson.toJson(error);
        }

        return gson.toJson(expense);
    }

    @GET
    @Path("all")
    public String getAllExpenses(){
        List<Expense> expenses = ExpenseDOM.getAllExpenses();
        return gson.toJson(expenses);
    }

    /**
     * Create a new trip and return it
     *
     * @param e
     * @return
     */
    @POST
    public String createExpense(String e) {
        Expense expense = gson.fromJson(e, Expense.class);
        Expense newExpense = ExpenseDOM.createNewExpense(expense.getUser(),
                expense.getTrip(), expense.getAmount(), expense.getDescription());

        if (newExpense == null) {
            ErrorModel error = new ErrorModel(404, "Resource not created!");
            return gson.toJson(error);
        }

        return gson.toJson(newExpense);
    }
}