package com.dwd.tripaccountantAPI.models;

import java.util.Date;

/**
 * Created by guilherme on 30/03/14.
 */
public class Trip {

    private String uuid;
    private String city;
    private String country;
    private Date startDate;
    private int numOfDays;
    private User user;

    public Trip(String uuid, String country, String city,
                Date startDate, int numOfDays, User user){
        this.uuid = uuid;
        this.user = user;
        this.country = country;
        this.city = city;
        this.startDate = startDate;
        this.numOfDays = numOfDays;
    }

    public User getUser(){
        return user;
    }

    public String getUuid() {
        return uuid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getNumOfDays() {
        return numOfDays;
    }

    public void setNumOfDays(int numOfDays) {
        this.numOfDays = numOfDays;
    }

}
