package com.dwd.tripaccountantAPI.models;

/**
 * Created by guilherme on 30/03/14.
 */
public class User {

    private String uuid;
    private String fullName;
    private String email;
    private String token;

    public User(){};

    public User(String uuid, String fullName, String email, String token){
        this.uuid = uuid;
        this.fullName = fullName;
        this.email = email;
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token){
        this.token = token;
    }

}