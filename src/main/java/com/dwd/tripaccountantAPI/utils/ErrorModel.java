package com.dwd.tripaccountantAPI.utils;

/**
 * Created by guilherme on 30/03/14.
 */
public class ErrorModel {
    private int httpErrorCode;
    private String msg;

    public ErrorModel(int httpErrorCode, String msg){
        this.setHttpErrorCode(httpErrorCode);
        this.setMsg(msg);
    }

    public int getHttpErrorCode() {
        return httpErrorCode;
    }

    public void setHttpErrorCode(int httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
