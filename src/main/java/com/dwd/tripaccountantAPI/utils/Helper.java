package com.dwd.tripaccountantAPI.utils;

import java.util.UUID;

/**
 * Created by guilherme on 31/03/14.
 */
public class Helper {
    public static String generateRandomUUID(){
        return UUID.randomUUID().toString();
    }
}
