package com.dwd.tripaccountantAPI;


import com.dwd.tripaccountantAPI.DOM.TripDOM;
import com.dwd.tripaccountantAPI.models.Trip;
import com.dwd.tripaccountantAPI.utils.ErrorModel;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Root resource (exposed at "expense" path)
 */
@Path("/trip")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TripAPI {

    private Gson gson = new Gson();

    /**
     * Get details about a specific trip
     * @param uuid
     * @return
     */
    @GET
    @Path("{uuid}")
    public String getTrip(@PathParam(value = "uuid") final String uuid) {

        Trip trip = null;

        if (uuid != null){
            trip = TripDOM.findTripById(uuid);
        }
        else{
            ErrorModel error = new ErrorModel(404, "Please specify either the UUID");
            return gson.toJson(error);
        }

        if(trip == null){
            ErrorModel error = new ErrorModel(404, "Resource not found!");
            return gson.toJson(error);
        }

        return gson.toJson(trip);
    }

    @GET
    @Path("all")
    public String getAllTrips(){
        List<Trip> trips = TripDOM.getAllTrips();
        return gson.toJson(trips);
    }

    /**
     * Create a new trip and return it
     * @param t
     * @return
     */
    @POST
    public String createTrip(String t){
        Trip trip = gson.fromJson(t, Trip.class);
        Trip newTrip = TripDOM.createNewTrip(trip.getUser(), trip.getCity(),
                trip.getCountry(), trip.getStartDate(), trip.getNumOfDays());

        if (newTrip == null){
            ErrorModel error = new ErrorModel(404, "Resource not created!");
            return gson.toJson(error);
        }

        return gson.toJson(newTrip);
    }
}