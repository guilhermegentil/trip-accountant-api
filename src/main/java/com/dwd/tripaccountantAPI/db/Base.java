package com.dwd.tripaccountantAPI.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by guilherme on 30/03/14.
 */
public class Base {

    static String dbUser = "tripaccountant";
    static String dbPassword = "tripaccountant";
    static String dbUrl = "jdbc:postgresql://127.0.0.1:5432/tripaccountant_api";

    static Connection dbConnection = null;

    /**
     *
     * @return
     */
    public static Connection getDbConnection(){
        try {
            if (dbConnection == null || dbConnection.isClosed()) {
                Class.forName("org.postgresql.Driver");
                dbConnection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
            }
        }
        catch (ClassNotFoundException ex) {
            System.err.println("ClassNotFoundException: " + ex.getMessage());
        }
        catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
        }
        return dbConnection;
    }

    public static void closeDbConnection(){
        try {
            if (dbConnection != null || !dbConnection.isClosed()) {
                dbConnection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
