package com.dwd.tripaccountantAPI;

import com.dwd.tripaccountantAPI.DOM.ExpenseDOM;
import com.dwd.tripaccountantAPI.DOM.TripDOM;
import com.dwd.tripaccountantAPI.DOM.UserDOM;
import com.dwd.tripaccountantAPI.models.Expense;
import com.dwd.tripaccountantAPI.models.Trip;
import com.dwd.tripaccountantAPI.models.User;
import com.dwd.tripaccountantAPI.utils.ErrorModel;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Root resource (exposed at "user" path)
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserAPI {

    private Gson gson = new Gson();

    /**
     * Return details about a user
     *
     * @param param
     * @return
     */
    @GET
    @Path("{param}")
    public String getUser(@PathParam(value = "param") final String param) {

        User user = null;

        if (param != null) {
            user = UserDOM.findUserById(param);
            if (user == null)
                user = UserDOM.findUserByEmail(param);
        } else {
            ErrorModel error = new ErrorModel(404, "Please specify either the UUID or EMAIL");
            return gson.toJson(error);
        }

        if (user == null) {
            ErrorModel error = new ErrorModel(404, "Resource not found!");
            return gson.toJson(error);
        }

        return gson.toJson(user);
    }

    /**
     * Get all the trips from a specific user.
     *
     * @param uuid
     * @return
     */
    @GET
    @Path("{uuid}/trips")
    public String getAllTripsFromUser(@PathParam("uuid") final String uuid){
        List<Trip> trips = TripDOM.getAllTripsByUserId(uuid);
        return gson.toJson(trips);
    }

    /**
     * Get all the expenses from a specific user.
     *
     * @param uuid
     * @return
     */
    @GET
    @Path("{uuid}/expenses")
    public String getAllExpensesFromUser(@PathParam("uuid") final String uuid){
        List<Expense> expenses = ExpenseDOM.getAllExpensesByUserId(uuid);
        return gson.toJson(expenses);
    }

    /**
     * Create  a new user and return it
     *
     * @param u
     */
    @POST
    public String createUser(String u) {
        User user = gson.fromJson(u, User.class);
        User newUser = UserDOM.createNewUser(user.getFullName(), user.getEmail());

        if (newUser == null) {
            ErrorModel error = new ErrorModel(404, "Resource not created!");
            return gson.toJson(error);
        }

        return gson.toJson(newUser);
    }
}